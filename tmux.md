# Tmux

`BK` - tzw. _bind-key_ czyli skrót klawiszowy, który wciskamy w sesji `Tmux`-a przed wciśnięciem dalej wymienionego klawisza. Domyślnie jest to `Ctrl+b`. Zatem `BK c` oznacza `Ctrl+b c`. `Bind-key` można zmienić na inny.

## Sesje
  * `tmux new -s mojasesja` - utworzenie nowej sesji o nazwie `mojasesja`
  * `tmux ls` - lista dostępnych sesji
  * `tmux a -t mojasesja` - dołączenie (ang. _attach_) do sesji `mojasesja`
  * `tmux kill-session -t mojasesja` - zabij sesję `mojasesja`
  * `BK , nazwa` - nadanie (nowej) nazwy sesji lub oknu
  * `BK d` - odłączenie się (ang. _detach_) od sesji. Sesja pozostaje aktywna w tle, nawet po wylogowaniu z serwera, można się do niej ponownie podłaczyć.
  * `BK x` - zamknięcie (zabicie) bieżącego okna lub sesji (jeśli to jest jedyne okno)
 
## Okna w sesji 

Okna w sesji `tmux`-a są domyślnie ponumerowane liczbami od 0 wzwyż. Jednak na klawiaturze `0` znjaduje się na końcu rzędu cyfr, a kolejna cyfra, `1`, na jej poczatku. Dlatego wygodnie jest zmienić to domyślne numerowanie tak, aby zaczynały się od 1.
Można to zrobić ustawiając odpowiedni parametr w pliku konfiguracyjnym `tmux`-a (`~/.tmux.conf`):
```
set -g base-index 1
```

  * `BK c` - utworzenie nowego okna
  * `BK , nazwa` - nadanie (nowej) nazwy oknu
  * `BK n` - następne okno
  * `BK p` - poprzednie okno
  * `BK NR` - przejście do okna o numerze NR
 
## Panele (ang. _panes_) w oknie

Okna można podzielić na mniejsze ,,okienka", zwane _panes_ co można przetłumaczyć jako panele. Tworzymy je przez dzielenie okna lub bieżączego panelu.

### Tworzenie paneli:

  * `BK %` podziel okno poziomo
  * `BK "` podziel okno pionowo

### Poruszanie się miedzy panelami

  * `BK strzałka` - poruszanie się między panalami zgodnie z kierunkiem strzałek na klawiaturze
  * `BK o` - następny panel
  * `BK ;` - poprzedni aktywny panel

### Zmiana rozmiaru i czasowe powiększanie panelu

  * `BK Ctrl+strzałka` - Zmiana wielkości panelu o 1 w kierunku skazanym przez strzałkę na klawiszu.
  * `BK Alt+strzałka` - Zmiana wielkości panelu o 5 w kierunku skazanym przez strzałkę na klawiszu.
  * `BK z` - bieżący panel zajmuje całe okno, ponowne zastosowanie `BK z` powoduje powrót do porzedniego ukladu



