# Konwersja strony z WordPress do Markdown

Narzędzia:
  - [Wordpress export to markdown](https://github.com/lonekorean/wordpress-export-to-markdown/)

1. Instalujemy (Ubuntu): 
```
sudo apt install npm
```
2. Eksportuję z Wordpress stronę (Narzędzia -> Export) wybieram „Wszystkie treści". Zapisuję plik w osobnym katalogu jako `export.xml`.
3. W katalogu z pobranym plikiem:
```
npx wordpress-export-to-markdown
```
Zgadzam się na instalację narzędzia i odpowiadam na pytania.



