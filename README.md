# TIPS

"Ściągi" do użytecznych programów (głównie skróty klawiszowe) i inne podręczne informacje, w trakcie (a raczej na początku) tworzenia.
Nie są to kursy, ani kompletne zbiory informacji na dany temat ale raczej moje notatki, tworzone pod kątem moich (!) zainteresowań oraz potrzeb i uzupełniane na bieżąco.
Może dla kogoś się okażą przydatne.

  * [Vim](vim.md)
  * [Tmux](tmux.md)
  * [Python](python.md)
  * [Bioinformatyka](bioinfo.md)
  * [LaTeX itp](tex.md)
  * [Docker](docker.md)
  * [VirtualBox](VirtualBox.md)
  * [NixOS](nixos.md)
  * [Książki, tutoriale, etc. dostępne online o różnej tematyce, głównie związane z Linuksem, programowaniem, bioinformatyką. Głównie w języku angielskim.](ksiazki.md)
  * [Eksport strony z Wordpress do Markdown](wp_do_md.md)
  * [Różne programy i narzędzia](narzedzia.md)


