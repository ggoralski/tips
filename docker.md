# Docker


## Instalacja Docker Desktop (w tym Docker Compose)

## Linux Mint, Ubuntu (?)

[https://www.linuxtechi.com/how-to-install-docker-on-linux-mint/?utm_content=cmp-true](https://www.linuxtechi.com/how-to-install-docker-on-linux-mint/?utm_content=cmp-true)

Jeśli:

	permission denied while trying to connect to the Docker daemon socket at unix:///var/run/docker.sock: Get "http://%2Fvar%2Frun%2Fdocker.sock/v1.24/version": dial unix /var/run/docker.sock: connect: permission denied

To: 

```
sudo chmod 666 /var/run/docker.sock
```

[https://stacktuts.com/got-permission-denied-while-trying-to-connect-to-the-docker-daemon-socket-at-unix-var-run-docker-sock-get-http-2fvar-2frun-2fdocker-sock-v1-24-version-dial-unix-var-run-docker-sock-connect-permission-denied](https://stacktuts.com/got-permission-denied-while-trying-to-connect-to-the-docker-daemon-socket-at-unix-var-run-docker-sock-get-http-2fvar-2frun-2fdocker-sock-v1-24-version-dial-unix-var-run-docker-sock-connect-permission-denied)

### Debian, MX Linux
Dodawanie repozytorium dla Debian bookworm:

```
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
  $(. /etc/os-release && echo "bookworm") stable" | \
  sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get update
```

Instalacja:

[https://docs.docker.com/engine/install/debian/#install-using-the-repository](https://docs.docker.com/engine/install/debian/#install-using-the-repository)

Doinstalować!

```
sudo apt-get install cgroupfs-mount
```

Uruchamianie demona:

```
sudo service docker start
```

Sprawdzenie:

```
sudo service docker status
```


### Błąd w łączeniu 

Jeśli pojawia się błąd:

    docker: Cannot connect to the Docker daemon at unix:///run/user/1000/docker.sock. Is the docker daemon running?.
    See 'docker run --help'.

To najpierw sprawdzić, czy działa z `sudo`. Jeśli tak, to:

Źródła: 
  - [https://phoenixnap.com/kb/cannot-connect-to-the-docker-daemon-error](https://phoenixnap.com/kb/cannot-connect-to-the-docker-daemon-error)
  - [https://www.howtogeek.com/devops/how-to-troubleshoot-cannot-connect-to-the-docker-daemon-errors/](https://www.howtogeek.com/devops/how-to-troubleshoot-cannot-connect-to-the-docker-daemon-errors/)

```
sudo groupadd -f docker
sudo usermod -aG docker $USER
```

Przelogować.

```
sudo service docker restart
```

```
docker context use default
```

## Czyszczenie

```
docker container rm -f $(docker container ls -aq)
```

Usuwanie kontenerów `diamol` z dysku:

```
docker image rm -f $(docker image ls -f reference='diamol/*' -q) 
```

## Różne polecenia:

`ID` - identyfikator kontenera, można zamiast `ID` używać nazwy (`nazwa`)

  - `docker polecenie --help` - pomoc do polecenia `polecenie` 
  - `docker run nazwa` - uruchomienie kontenera o nazwie `nazwa`
  - `docker run --rm nazwa` - uruchomienie kontenera `nazwa` i usunięcie go, gdy przestanie działać (?)
  - `docker run -d nazwa` - uruchomienie i odłączenie `nazwa` - czyli uruchamia się jako demon
  - `docker stop nazwa` - zatrzymanie kontenera `nazwa`
  - `docker pause nazwa` - zapauzowanie (czasowe wstrzymanie) `nazwa`
  - `docker unpause nazwa` - odpauzowanie (wznowienie) `nazwa`
  - `docker container ls` - lista działających kontenerów
  - `docker container ls --all` - wszystkie dostępne kontenery, niezależnie od statusu
  - `docker container ps` - lista działających kontenerów
  - `docker container ps -a` - lista działających kontenerów
  - `docker container top ID` - lista procesów w kontenerze o identyfikatorze ID (lub jego początek?)
  - `docker container logs ID ` - wpisy w rejestrze zapisane przez kontener
  - `docker container inspect ID` - szczegółowe informacje o kontenerze
  - `docker container stats` - wykorzystanie zasobów przez kontenery
  - `docker container rm -f ID` - Usuwanie kontenera o danym ID 
  - `docker container export -o plik.tar ID` - eksport systemu plików z kontenera ID do pliku `.tar`.
  - `docker cp sciezka/plik_lokalny ID:sciezka_kontenera` - kopiowanie lokalnego pliku do kontenera ID. Uwaga: `*` nie działa przy kopiowaniu. Można to obejść np tak: `docker exec container bash -c "ls home/*.png" | while read line; do docker cp container:/$line .; done` (niesprawdzone)
  - `docker container exec ID komenda` - uruchamianie komendy na kontenerze ID.
  - `docker image pull obraz` - pobranie obrazu kontenera. 
  - `docker image build -t nazwa:[tag]` - budowanie obrazu
  - `docker container run -d --name nazwa obraz` - uruchomienie pobranego kontenera (`-d, --detach` - uruchamianie w tle)
  - `docker container logs ID` - logi kontenera
  - `docker rm -f ID` - usuwanie kontenera
  - `docker container run --env ZMIENNA=wartość obraz` - uruchamianie obrazu wraz z ustawieniem wartości zmiennej środowiskowej.
  - `docker container commit ID nazwa` - kontener jest zapisywany jako nowy obraz, przechowywany lokalnie.
  - `docker images` - dostępne lokalnie obrazy
  - `docker diff nazwa` - zmiany w obrazie `nazwa`

## Odłączanie i ponowne podłączanie do kontenera

  - `Ctrl+P Ctrl+Q` - odłączanie od kontenera
  - `docker attach kontener` - podłączanie do kontenera o nazwie `kontener` (nazwa znajduje się w kolumnie `NAMES` w wyniku działania komendy `docker container ps`). 


## Polecenia związane z Docker Hub

  - `docker search nazwa` - wyszukiwanie obrazów związanych z `nazwa` 
  - `docker search --no-trunc nazwa` - wyszukiwanie obrazów związanych z `nazwa`, bez przycinania wyników 

## Różne

  - `docker run -p 10000:8888 quay.io/jupyter/scipy-notebook:2024-01-15` - uruchomienie `scipy-notebook`
  - `docker run -e GRANT_SUDO=yes --user root -p 10000:8888 quay.io/jupyter/scipy-notebook:2024-01-15` - j.w. z prawami roota
  Jupyter otwieramy w przegladarce pod adresem `http://127.0.0.1:10000/?token=TOKEN`, gdzie `TOKEN` to token, który wyświetla się w wyniku uruchamiania.



