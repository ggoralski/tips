# Książki, tutoriale etc. dostępne on-line

Poniżej znajdują się linki do książek (itp.) dostępnych on-line. Wg. mojej wiedzy wszystkie są dostępne on-line legalnie, choć niekoniecznie darmowe.

## Listy książek, tutoriali etc. 

  * [Free programming books](https://github.com/lilbruddr/free-programming-books/blob/master/free-programming-books.md)
  * [Free Learning Resources In Many Languages](https://github.com/EbookFoundation/free-programming-books)

## Linux, linia komend, powłoki

  * Dwie książki poświęcone linii komend w Linuksie. Do pobrania w formacie pdf. Linki pochodzą ze strony [https://linuxcommand.org/](https://linuxcommand.org/), warto sprawdzić czy nie ma nowszych wersji.
    * [The Linux Command Line](https://sourceforge.net/projects/linuxcommand/files/TLCL/19.01/TLCL-19.01.pdf/download)
    * [Adventures with the Linux Command Line](https://sourceforge.net/projects/linuxcommand/files/AWTLCL/21.10/AWTLCL-21.10.pdf/download)
  * [Sztuka używania wiersza poleceń (w wielu językach)](https://github.com/jlevy/the-art-of-command-line/blob/master/README-pl.md)
  * [Wombat’s Book of Nix](https://mhwombat.codeberg.page/nix-book/)

## Tmux

  * [The tao of tmux](https://leanpub.com/the-tao-of-tmux/read)

## Python

  * [Automate the Boring Stuff with Python](https://automatetheboringstuff.com/#toc)

## R
  * [https://trevorfrench.github.io/R-for-Data-Analysis/](https://trevorfrench.github.io/R-for-Data-Analysis/)

## Analiza danych

  * [A Portable Introduction to Data Analysis](https://uq.pressbooks.pub/portable-introduction-data-analysis/)

## Inne

  * [Design resources for developers](https://github.com/bradtraversy/design-resources-for-developers)


