# Bioinformatyka

## Zapis modeli substytucji w programie [MrBayes](https://nbisweden.github.io/MrBayes/).

Źródło: [https://gist.github.com/brantfaircloth/895282](https://gist.github.com/brantfaircloth/895282)

| Zapis w MrBayes | Model |
| --- | --- |
| |**GTR** |
|`lset applyto=() nst=6                          ` | GTR |
|`lset applyto=() nst=6 rates=propinv            ` | GTR + I |
|`lset applyto=() nst=6 rates=gamma              ` | GTR + gamma |
|`lset applyto=() nst=6 rates=invgamma           ` | GTR + I + gamma |
| | |
| |**SYM**|
|`lset applyto=() nst=6` <br> `prset applyto=() statefreqpr=fixed(equal)` | SYM|
|`lset applyto=() nst=6 rates=propinv` <br> `prset applyto=() statefreqpr=fixed(equal)` | SYM + I|
|`lset applyto=() nst=6 rates=gamma` <br> `prset applyto=() statefreqpr=fixed(equal)` | SYM + gamma|
|`lset applyto=() nst=6 rates=invgamma` <br> `prset applyto=() statefreqpr=fixed(equal)` | SYM + I + gamma|
|    | |
| |**HKY**|
|`lset applyto=() nst=2                          ` | HKY|
|`lset applyto=() nst=2 rates=propinv            ` | HKY + I|
|`lset applyto=() nst=2 rates=gamma              ` | HKY + gamma|
|`lset applyto=() nst=2 rates=invgamma           ` | HKY + I + gamma|
| | |
| |**K2P**|
|`lset applyto=() nst=2` <br> `prset applyto=() statefreqpr=fixed(equal)` | K2P|
|`lset applyto=() nst=2 rates=propinv` <br> `prset applyto=() statefreqpr=fixed(equal)`| K2P + I|
|`lset applyto=() nst=2 rates=gamma` <br> `prset applyto=() statefreqpr=fixed(equal)` | K2P + gamma|
|`lset applyto=() nst=2 rates=invgamma` <br> `prset applyto=() statefreqpr=fixed(equal)` | K2P + I + gamma|
| | |
| |**F81**|
|`lset applyto=() nst=1                          ` | F81|
|`lset applyto=() nst=1 rates=propinv            ` | F81 + I|
|`lset applyto=() nst=1 rates=gamma              ` | F81 + gamma|
|`lset applyto=() nst=1 rates=invgamma           ` | F81 + I + gamma|
| | |
| |**Jukes Cantor**|
|`lset applyto=() nst=1` <br> `prset applyto=() statefreqpr=fixed(equal)`| JC|
|`lset applyto=() nst=1 rates=propinv` <br> `prset applyto=() statefreqpr=fixed(equal)` | JC + I|
|`lset applyto=() nst=1 rates=gamma` <br> `prset applyto=() statefreqpr=fixed(equal)`| JC + gamma|
|`lset applyto=() nst=1 rates=incgamma` <br> `prset applyto=() statefreqpr=fixed(equal)` | JC + I + gamma|
|    | |


## BLAST

Bazy BLAST-a:

```
update_blastdb --showall
```

Blast na bazie `refseq_genomic`

```
blastn -db refseq_genomic -query seq.fasta -out results2.out -remote
```

## Różne

### Usuwanie duplikatów (takich samych sekwencji, bez znaczenia jaką mają nazwę)

```
seqkit rmdup -s < file_in.fasta > file_out.fasta
```