# Python

## `python2` i `python3`

### Z `Conda`

Tworzenie środowiska `python2:
```
conda create --name ggpy2 python=2.7
conda activate ggpy2
conda install scipy
conda install -c bioconda mathstats 
conda install -c bioconda pysam 
```
Potem: 
```
conda activate ggpy2`
```

### Przełączanie się pomiędzy wersjami Pythona, bez condy:

Zainstalować w razie potrzeby wersje pythona za pomoca `apt`.

Sprawdzenie wersji pythona:

```sh
ls /usr/bin/python*
```

Potem:

Na początku, raz:

```sh
sudo update-alternatives --list python
sudo update-alternatives --install /usr/bin/python python /usr/bin/python2 1
sudo update-alternatives --install /usr/bin/python python /usr/bin/python3 2
```

Potem:

```sh
sudo update-alternatives --config python
```


## Emaile itp

  * [Instrukcja ysyłania e-maila z konta Gmail i SMS ClarityCoders w Youtube](https://www.youtube.com/watch?v=B1IsCbXp0uE)
  * [Instrukcja obsługi email-i i wysyłanie SMS-ów w książce "Automate the Boring Stuff with Python"](https://automatetheboringstuff.com/2e/chapter18/)
  * [Jeszcze inna instrukcja](https://github.com/CoreyMSchafer/code_snippets/blob/master/Python/Emails/mail-demo.py)

```python
import os
import smtplib
import imghdr
from email.message import EmailMessage

EMAIL_ADDRESS = os.environ.get('EMAIL_USER')
EMAIL_PASSWORD = os.environ.get('EMAIL_PASS')

contacts = ['YourAddress@gmail.com', 'test@example.com']

msg = EmailMessage()
msg['Subject'] = 'Check out Bronx as a puppy!'
msg['From'] = EMAIL_ADDRESS
msg['To'] = 'YourAddress@gmail.com'

msg.set_content('This is a plain text email')

msg.add_alternative("""\
<!DOCTYPE html>
<html>
    <body>
        <h1 style="color:SlateGray;">This is an HTML Email!</h1>
    </body>
</html>
""", subtype='html')


with smtplib.SMTP_SSL('smtp.gmail.com', 465) as smtp:
    smtp.login(EMAIL_ADDRESS, EMAIL_PASSWORD)
    smtp.send_message(msg)
```

