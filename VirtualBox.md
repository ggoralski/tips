# VirtualBox

## Zmiana wielkości wirtualnego dysku z systemem Linux (na własną odpowiedzialność!)

1. Sprawdzam, jakie są parametry wirtualnego dysku:
```
VBoxManage showhdinfo ścieżka/do/dysku.vdi
```
2. Zmieniam rozmiar wirtualnego dysku, np. na 40GB 
```
VBoxManage modifyhd ścieżka/do/twojego/dysku.vdi --resize 40960
```

### Opcja 1

2. Instaluję gparted (jeśli nie jest zaistalowany)
```
sudo apt install gparted
```

3. W `gparted` zwiększam rozmar partycji

### Opcja 2

3. Włączam system wirtualny. Uruchamiam `parted`, tu dla dysku `sda`:
```
sudo parted /dev/sda
```
4. Zmieniam rozmiar partycji 1 (sda1) na 40GB
```
resizepart 1 40GB
```
5. Rozszerzam system plików:
```
sudo resize2fs /dev/sda1
```


