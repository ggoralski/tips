# Różne programy, narzędzia

## Markdown, konwersje formatów
  - [mdBook](https://rust-lang.github.io/mdBook/index.html) - narzędzie do tworzenia książek (stron internetowych) z plików Markdown.
  - [Wordpress export to markdown](https://github.com/lonekorean/wordpress-export-to-markdown/) - Eksport stron z Wordpress do Markdown (zob. [Konwersja strony z WordPress do Markdown](wp_do_md.md))
  
