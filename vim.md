# VIM

## Podstawowe tryby

  * Tryb normalny (_normal_) - wbrew intuicji nie wpisujemy w nim bezpośrednio tekstu, ale przy pomocy klawiszy wydajemy komendy, które służą m. in. poruszaniu się, usuwaniu czy kopiowaniu tekstu. Po uruchomieniu `vim` znajduje się w tym trybie, przenosimy się do niego z innych trybów za pomocą klawisza `<Esc>`, czasem trzeba go wcisnąć kilkukrotnie.
  * Tryb edycji (_insert_) - to w tym trybie wprowadzamy tekst, niektóre komendy są także dostępne za pomocą skrótów klawiszowych. Z trybu normalnego wchodzimy do niego używając odpowiednich poleceń (np. `i` - wprowadź tekst w miejscu kursora, `A` - wprowadź tekst na końcu linii)
  * Tryb linii komend/EX (_command line/ex_) - służy wprowadzaniu komend i wyszukiwaniu tekstu. Uruchamiamy go z trybu normalnego klawiszami `:` w celu wpisania komendy, `/` lub `?` aby wyszukać tekst, `!` żeby filtrować tekst. Jeśli znajdujesz się w innym, niż normalny, trybie, najpierw należy wejść do trybu normalnego (`<Esc>`). Na dole pokazuje się linia, do której przeniesiony jest kursor, tam wpisujemy komendy. Spróbuj `:help vim-modes`. W tym trybie m.in. wykonuje się komendy wyjścia z programu, otwieranie i zapisywanie pliku, zamianę tekstu czy zmiany ustawień programu.

## Podstawowe klawisze i komendy

Kilka przydatnych klawiszy i komend do poruszania się w trybie _normal_:

  * `w` - następne słowo (ang. _word_)
  * `b` - poprzednie słowo (ang. _backward_)
  * `0` - początek linii
  * `$` - koniec linii
  * `)` - następne zdanie
  * `(` - poprzednie zdanie
  * `}` - następny paragraf
  * `{` - poprzedni paragraf
  * `gg` - początek pliku
  * `G` - koniec pliku
  * `t?` - przeniesienie do najbliższego (w prawo) znaku poprzedzającego określony znak w bieżącej linii (zamiast `?` dowolny znak). 

Podając liczbę, można zmienić miejsce docelowe, np:

  * `5w` - pięć słów naprzód
  * `5G` - piąta linia

Kilka poleceń edycyjnych w trybie _normal_:

  * `x` - usuń znak
  * `yy` - kopiuj linię (ang. _yank_)
  * `yw` - kopiuj słowo (ang. _yank word_)
  * `dd` - usuń linię (ang. _delete_)
  * `dw` - usuń do końca słowo (ang. _delete word_)
  * `daw` - usuń całe słowo (ang. _delete all word_) włącznie z białymi znakami (np. spacje) po wyrazie i lub przed wyrazem (kiedy nie ma białego znaku po wyrazie) 
  * `diw` - usuń całe słowo bez spacji
  * `ciw` - zmień całe słowo bez spacji (polecenie usuwa słowo i wchodzi w tryb _insert_)
  * `d$` - usuń tekst do końca linii
  * `p` - wklej po kursorze (poniżej) (ang. _paste_)
  * `P` - wklej przed kursorem (powyżej)
  * `J` - połącz linię z następną (ang. _Join_)
  * `u` - cofnij zmianę lub edycję (ang. _undo_)
  * `<Ctrl>-r` - cofnięcie cofnięcia (ang. _redo_)
  * `yt?` - kopiuj tekst do znaku `?` (dowolny znak, znak ten nie jest kopiowany) 
  * `yf?` - kopiuj tekst do znaku `?` (dowolny znak, znak ten jest kopiowany) 

Jak widać, przynajmniej niektóre, polecenia edycji można łączyć z komendami poruszania się. Polecenia takie jak `d` czy `y` trzeba uzupełnić o informację o ruchu. Można je także dodatkowo uzupełnić o liczbę:

  * `4J` - połącz cztery linie
  * `5dw` - usuń pięć kolejnych słów
  * `5yy` - skopiuj pięć linii

Kilka podstawowych komend (w trybie _command line_):

  * `:e nazwa_pliku` - otwórz plik
  * `:w` - zapisz plik
  * `:wq` - zapisz wszystkie pliki otwarte w panelach
  * `:q` - wyjdź (jeśli zmiany w pliku nie zostały zapisane, wyjście nie jest możliwe)
  * `:q!` - wyjdź mimo niezapisania zmian
  * `:wq` - zapisz zmiany w pliku i wyjdź
  * `:w nowa_nazwa` - zapisz plik pod nową nazwą (zapisana jest kopia pliku)
  * `:saveas nowa_nazwa` - zapisz plik pod nową nazwą i kontynuuje edycję pod nową nazwą
  * `:r inny_plik` - wczytaj zawartość innego pliku i wstaw go w bieżącym (poniżej kursora)

## Tryb Visual

W trybie _visual_ dokonujemy zaznaczenia tekstu,
Wchodzimy do niego z trybu _normal_ poprzez klawisze: `V`, `v` lub `Ctrl+v`. Następnie poruszając się po tekście zaznaczamy fragment, na którym wykonujemy określone czynności wydając polecenia.

Przykłady:

  * `V` - zaznaczanie linii
  * `v` - zaznaczanie znaków
  * `Ctrl+v` - zaznaczanie blokowe
  * `gv` - zaznacz ponownie poprzednie zaznaczenie
  
Zaznaczanie można łączyć np. z poleceniami ruchu

  * `5V` - zaznacz 5 linii  
  * `V365G` -  zaznacz linie w dół do linii 365
  * `V20gg` - zaznacz linie w górę do linii 20
  * `vtX` - zaznacz znaki w linii do znaku `X` (bez `X`)
  * `v/aaa` - zaznacz do ciągu znaków `aaa` (bez `aaa`)

Wstawianie/zamiana tekstu w zaznaczonym bloku:

  1. <Ctrl+v>
  2. Rozciągnięcie zaznaczenia blokowego (np. dwa znaki na prawo i 5 linni w dół)
  3. Wejście w edycję (jenda z wymienionych możliwości):
      - `A` - wstawianie na końcu zaznaczenia
      - `I` - wstawianie na początku zaznaczenia
      - `c` - zamiana zaznaczonego tekstu
  4. Wpisanie nowego tekstu
  5. \<Esc\>

## Panele (okna)

Okno z otwartym programem vim można podzielić na mniejsze "okienka", które będe nazywał „panelami”. Je też możemy dalej dzielić.

### Podział okna/panelu:

  * `:[NR]sp [plik]` - podział panelu poziomo - `NR` to liczba linii w nowym panelu, np:
  `:20sp tekst.txt`
  * `:[NR]vs[p] [plik]` - podział panelu pionowo - `NR` to liczba znaków (kolumn) w nowym panelu, np:
  `:20vs lista.txt`

Jeśli nie podamy `NR` to panel podzieli się równo.
Jeśli nie podamy nazwy pliku to w obu panelach będzie otwarty ten sam plik.

### Poruszanie się między panelami:

Ogólnie poruszanie się między panelami polega na wciśnięciu `Ctrl-w` a potem klawisza odpowiedzialnego za ruch (`h, j, k, l`) lub strzałki.

  * `Ctrl-w h` - zmiana na panel po lewej
  * `Ctrl-w j` - zmiana na panel poniżej
  * `Ctrl-w k` - zmiana na panel powyżej
  * `Ctrl-w l` - zmiana na panel po prawej

### Zamykanie paneli

Każde panel z osobna zamyka się tak jak `vim`-a (np `:wq`). 
Można zamknąć wszystkie panelu poza tym, w którym aktualnie pracujemy:

  * `:on` - (ang. _only_) - zamknięcie wszystkich innych paneli o ile są zapisane
  * `:on!` - zamknięcie innych paneli bez względu na to, czy są zapisane.

### Zmiana wielkości panelu

  * `Ctrl+w =` - Zmień wielkość paneli na równy
  * `Ctrl+w _` - maksymalizacja wysokości bieżącego panelu
  * `Ctrl+w |` - maksymalizacja szerokości bieżącego panelu
  * `Ctrl+w [NR] >`, `Ctrl+w [NR] <` - Zmiana wielkości panelu w kierunku prawym (`>`) i lewym (`<`), `[NR]` - opcjonalna liczba określająca wielkość zmiany   
  * `Ctrl+w [NR] +`, `Ctrl+w [NR] -` - Zwiększanie i zmniejszanie wysokości panelu w pionie, `[NR]` - opcjonalna liczba określająca wielkość zmiany.

### Zmiana ułożenia paneli

  * `Ctrl+w r` - przesunięcie panelu w prawo, jeśli panelu są ułożone w rzędzie, lub w dół, jeśli ułożone w kolumnie
  * `Ctrl+w R` - przesunięcie panelu w lewo, jeśli panelu są ułożone w rzędzie, lub w górę, jeśli ułożone w kolumnie
  * `Ctrl+w H|J|K|L` - Przesunięcie panelu na skraj lewy (`H`), dolny (`J`), górny (`K`) lub prawy (`L`) i zajęcie całego dostępnego miejsca w pionie (`H|L`) lub poziomie (`J|K`).
  
## Karty (inaczej: taby, zakładki)

### Tworzenie, zamykanie kart

  * `:tabf plik` - otwórz `plik` w nowej karcie
  * `:tabe plik` - otwórz `plik` w nowej karcie
  * `:tabc` - zamknij kartę
  * `:tabo` - zamknij inne karty, zachowaj bieżącą kartę
  * `<Ctrl+w T> - przenieś bieżący bufor (panel/okno) do nowej karty

### Poruszanie się między kartami

  * `gt` lub `:tabn` - przejdź do następnej karty
  * `gT` lub `:tabp` - przejdź do poprzedniej karty
  * `Ngt` lub `:tabn N` - przejdź do karty o nr. `N`
  
## Bufory

Jeśli otworzymy `vim`-a np tak: `vim *.txt` to w pamięci programu może być otwartych wiele plików w tzw. buforach, ale tylko pierwszy z nich będzie widoczny. Możemy się między tymi plikami przemieszczać i je np. edytować.

  * `:ls` - wydrukuj listę buforów, bufor otwarty w biezacym oknie jest oznaczony `%`, alternatywny znakiem `#`.
  * `:bN` - przełącz się na bufor o numerze N
  * `:bn` - przełącz się na następny bufor
  * `:bp` - przełącz się na poprzedni bufor
  * `:bf` - przełącz się na pierwszy bufor
  * `:bl` - przełącz się na ostatni bufor
  * `:ba` - otwórz wszystkie bufory w panelach (w pionie)
  * `:bd` - zamknij bieżący bufor
  * `:bd N` - zamknij bufor o numerze N
  * `:sb N` - podziel okno poziomo i otwórz bufor o numerze N w nowym panelu
  * `:sb nazwa` -  podziel okno poziomo i otwórz bufor o nazwie `nazwa`  w nowym panelu
  * `:sbn` -  podziel okno poziomo i otwórz następny bufor w nowym panelu
  * `:sbp` -  podziel okno poziomo i otwórz poprzedni bufor w nowym panelu
  * `:sba` - otwórz wszystkie bufory w panelach
  * `<Ctrl+^>` - przełącz się na alternatywny bufor (oznaczony znakiem `#`)

## Sesje

Ułożenie paneli i to jakie pliki są w nich otwarte można zapisać jako **sesje**.

### Zapisanie sesji
  * `:mksession nazwa_sesji.vim` - zapisanie sesji w pliku `nazwa_sesji.vim`

### Otwarcie zapisanej sesji
  * (w terminalu): `vim -S nazwa_sesji.vim` - załadowanie zapisanej sesji przy uruchamianiu vim-a.
  * (w vim-ie): `:source nazwa_sesji.vim`

## Makra

W vim-ie można nagrywać makra, czyli ciągi czynności a następnie je odtwarzać. Makro przypisujemy do litery. Następnie używając tej litery uruchamiamy dane makro. Można jednocześnie utworzyć wiele makr, przypisując je do różnych liter. Nagrywanie makra rozpoczynamy przechodząc z trybu _normal_ w tryb makra za pomocą klawisza `q`. Następnie wykonujemy kolejne polecenia, nagrywanie kończymy ponownie wciskając w trybie _normal_ klawisz `q`. Makro wywołujemy używając `@LITERA`, gdzie `LITERA` oznacza oczywiście literę, którą przypisaliśmy do makra. Zatem, jeśli przyjmiemy, że `LITERA` to `a`:

  1. `qa` (w trybie _normal_) - rozpoczęcie nagrywania makra i przypisanie go do litery `a`.
  2. Wykonywanie czynności, które nagrywamy.
  3. `q` (w trybie _normal_) - wyjście z nagrywania

Następnie wywołujemy makro:
  * `@a` - wywołanie makra przypisanego do `a`
  * `@@` - wywołanie ostatnio wywołanego makra, niezależnie do jakiej litery było przypisane.

## Rejestry

Rejestry umożliwiają zapamiętanie i następnie np. wklejanie tekstu, czyli działają na zasadzie ,,schowka''. Można jedocześnie używać wielu rejestrów, kazdy z nich ma swoją jednoliterową nazwę. 

Jeśli umieszczamy tekst w rejestrze używając małejlitery (np `a`) to nowy tekst zastepuje poprzedni zapamiętany w tym rejestrze, jęśli używamy dużej litery, to nowy tekst jest dołączany do poprzedniej zawartości. 

Do rejestrów odwołujemy się za pomoca znaku `"`, czyli np. `"a` odwołuje się do rejestru `a`. 

Niektóre szczególne rejestry:

  * `""` - domyślny rejestr, do którego jest zapisywany tekst jeśli nie używamy nazwy rejestru.
  * `"+` - rejest przechowujący tekst ze schowka systemowego (np. po skopiowaniu tekstu z innego programu)
  * `"*` - jak wyżej, ale w systemie Linux są pewne różnice
  * `":` - ostatnia komenda (wydana w trybie _command_)
  * `"%` - zawiera nazwę bieżącego pliku
 
Przykłady:

  * `"ayiw` - skopiuj całe bieżące słowo do rejestru `a`, zastępując jego zawartość
  * `"Add` - usuń bieżącą linię i dołącz ją do rejestru `a`.
  * `"iap` - wklej zawartość rejestru `a` w miejsce kursora
  * `"+yy` - skopiuj bieżącą linię do systemowego schowka
  * `"+p` - wklej zawartość systemowego schowka
  * `":p` - wklej ostatnie polecenie wydane w trybie _command_ 

## Polecenie `g`

Polecenie `g` (tryb _command_) pozwala odnajdywać linie zawierające wzorce i coś z nimi robić.

Przykłady:

  * `:g/wzorzec/d` - usuń linie zawierające `wzorzec`
  * `:g!/wzorzec/d` lub `v/wzorzec/d` - usuń linie **nie zawierające** `wzorzec`
  * `:g/wzorzec/t$` - kopiuj linie zawierające `wzorzec` na koniec pliku
  * `:g/wzorzec/m0` - przenieś linie zawierające `wzorzec` na początek pliku
  * `:g/wzorzec/m5` - przenieś linie zawierające `wzorzec` do miejsca w pliku zaczynającego się oj linii nr. **6**.
  * `:g/wzorzec/ s/X/Y/g` - zamień `X` na `Y` w liniach zawierających `wzorzec`.


## _Digraphs_ - wprowadzanie „nietypowych” znaków

W trybie _insert_ możemy wprowadzić do tekstu różne ,,nietypowe" znaki, których nie możemy wporwadzić wprost z klawiatury.

W tym celu najpierw wciskamy `<Ctrl+k>` a potem dwa znaki (_digraph_), które ,,kodują'' odpowiedni znak.

Pełną listę tych ,,dwuznaków'' można obejrzeć używając komendy:
` :h digraph-table`, albo na stronie [https://vimhelp.org/digraph.txt.html](https://vimhelp.org/digraph.txt.html)

Przykładowe ,,dwuznaki'' (wciskamy klawisze po `<Ctrl+k>`):

| **Znak** | **Dwuznak** |
| ---- | ------- |
| `„`  | `:9`    |
| `”`  | `"9`    |
| `→`  | `->`    |
| `←`  | `-<`    |
| `↑`  | `-!`    |
| `↓`  | `-v`    |
| `↔`  | `<>`    |
| `↕`  | `UD`    |
| `°`  | `DG`    |
| `℃`  | `oC`    |
| `±`  | `+-`    |
| `µ`  | `My`    |
| `Π`  | `P*`    |
| `π`  | `p*`    |
| `Σ`  | `S*`    |
| `α`  | `a*`    |
| `₁`  | `1s`    |
| `¹`  | `1S`    |
| `⁺`  | `+S`    |
| `⁻`  | `-S`    |
| `ⁿ`  | `nS`    |
| `½`  | `12`    |
| `⅔`  | `23`    |
| `♂`  | `Ml`    |
| `♀`  | `Fm`    |
| `✓`  | `OK`    |
| `✗`  | `XX`    |
| `≤`  | `=<`    |
| `≥`  | `>=`    |
| `∞`  | `00`    |
| `∙`  | `Sb`    |
| `±`  | `+-`    |
| `≠`  | `!=`    |


## Zmiana polskich liter na podstawowe łacińskie

W trybie _command_:

```
:%s/[ąćęłńóśźżĄĆĘŁŃÓŚŹŻ]/\=submatch(0)=='ą'?'a':submatch(0)=='ć'?'c':submatch(0)=='ę'?'e':submatch(0)=='ł'?'l':submatch(0)=='ń'?'n':submatch(0)=='ó'?'o':submatch(0)=='ś'?'s':submatch(0)=='ź'?'z':submatch(0)=='ż'?'z':submatch(0)=='Ą'?'A':submatch(0)=='Ć'?'C':submatch(0)=='Ę'?'E':submatch(0)=='Ł'?'L':submatch(0)=='Ń'?'N':submatch(0)=='Ó'?'O':submatch(0)=='Ś'?'S':submatch(0)=='Ź'?'Z':submatch(0)=='Ż'?'Z':'z'/g
```

## Różne ustawienia w `~/.vimrc`

```
" Przypisz Ctrl+Shift+V do wklejania ze schowka
nnoremap <C-S-v> "+p
vnoremap <C-S-v> "+p
inoremap <C-S-v> <C-r>+

" Przypisz Ctrl+Shift+C do kopiowania do schowka
vnoremap <C-S-c> "+y
```

## Różne

  * `gg=G` - automatyczne formatowanie wcięć kodu
  * `gf` - przejście do pliku którego nazwa znajduje się pod kursorem (otwiera się zamiast otwartego pliku). Powrót do poprzedniego pliku <Ctrl+o>
  * `:<Ctrl+r>+` - wklejanie do linii komend tekstu ze schowka
  * Wstawianie w co dziesiątej linii `---` (w linii komend):
    `:for i in range(10, line('$'), 10) | call append(i, '---') | endfor`
  
