# TeX, LaTeX...

## Automatyczne formatowanie kodu

### `latexindent`

Informacje o uruchamianiu itd: [https://tex.stackexchange.com/questions/562904/automatic-latex-code-formatting-similar-to-clang-format](https://tex.stackexchange.com/questions/562904/automatic-latex-code-formatting-similar-to-clang-format)

Strona projektu: [https://github.com/cmhughes/latexindent.pl](https://github.com/cmhughes/latexindent.pl)

Instalacja z użyciem `conda`:

```
conda create -n latexindent.pl
conda activate latexindent.pl
conda install latexindent.pl -c conda-forge
conda info --envs
conda list
conda run latexindent.pl -vv
```

Podstawowe użycie:

```
latexindent -w -s 16-genbank-www.latex
```

  - `w` - modyfikacja pliku 
  - `s` - bez drukowania w terminalu
