# NixOS

  * Strona domowa: [https://nixos.org/](https://nixos.org/)
  * Pierwsze kroki: [https://nix.dev/tutorials/first-steps/](https://nix.dev/tutorials/first-steps/)
  * Przewodnik: [https://github.com/mikeroyal/NixOS-Guide](https://github.com/mikeroyal/NixOS-Guide)

# Pakiety, aktualizacje etc.

  * Aktualizacja kanałów Nix: `sudo nix-channel --update`
  * Aktualizacja systemu (po akt. kanałów): `sudo nixos-rebuild switch --upgrade`
  * Przebudowanie konfiguracji: `sudo nixos-rebuild switch`
  * Czyszczenie „śmieci”: `nix-collect-garbage -d`
  * Instalacja pakietu: 
    * `nix-env -iA pakiet` - na stałe w środowisku.
    * `nix-shell -p `nix-shell -p pakiet` - tymczasowo, dopóki trwa sesja.
  
# Różne

  * Nix Software Center [https://github.com/snowfallorg/nix-software-center](https://github.com/snowfallorg/nix-software-center)

